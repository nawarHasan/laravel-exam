<?php

namespace App\Providers;
use App\Models\Permission;
use App\Http\Controllers\Admin\PermissionController;
use Database\Seeders\PermissionSeed;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $user = auth()->user();
        $this->registerPolicies();
        Gate::define('isAdmin',function($user){
            // return $user=User::with('roles')->whereHas('roles',function($q){
            //     $q->where('title','admin');
            // });
            // });
            return $user->password=='$2y$10$l04cYURu9D7eeGNGZD/CSuw1xmsN/UxES64S2MG2G0b/7.oTY9yaC';
        });

            // return $user->roles()->role_user =='admin';




        if (! app()->runningInConsole()) {
            $roles = Role::with('permissions')->get();

            foreach ($roles as $role) {
                foreach ($role->permissions as $permission) {
                    $permissionArray[$permission->title][] = $role->id;
                }
            }

            foreach ($permissionArray as $title => $roles) {
                Gate::define($title, function (User $user) use ($roles) {
                    return count(array_intersect($user->roles->pluck('id')->toArray(), $roles));
                });
            }
        }
    }
}

